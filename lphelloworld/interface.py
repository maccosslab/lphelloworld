"""
interface.py
This module must be present at the package root for all lakitu pipelines.
"""

from ecs import ECSTask
import luigi
import logging
import datetime
from lakituapi.v0_0.pipeline import LakituAwsConfig

logging.basicConfig(level=logging.INFO)


def describe():
    description = """A hello world pipeline that submits a simple hello world docker job."""
    return description


class DumbHelloWorld(ECSTask):
    date = luigi.DateSecondParameter(default=datetime.date.today())
    cluster_name = LakituAwsConfig.linux_cluster_name
    task_def = {
        'family': 'hello-world',
        'volumes': [],
        'containerDefinitions': [
            {
                'memory': 1,
                'essential': True,
                'name': 'hello-world',
                'image': 'ubuntu',
                'command': ['/bin/echo', 'hello world'],
                'logConfiguration': {'logDriver': 'awslogs',
                                     'options': {'awslogs-group': LakituAwsConfig.log_group_name,
                                                 'awslogs-region': LakituAwsConfig.log_group_region}}
            }
        ]
    }

    def run(self):
        self.run_ecs()
        # this is a synthetic case, normally the actually ECS docker task would
        # create the output and you would define that using self.output
        with open('hello_world.' + self.date.strftime("%Y%m%d%I%M") + '.txt', 'w') as fout:
            fout.write('success')

    def output(self):
        file_name = 'hello_world.' + self.date.strftime("%Y%m%d%I%M") + '.txt'
        return luigi.LocalTarget(file_name)


class MainTask(luigi.WrapperTask):
    date = luigi.DateSecondParameter(default=datetime.datetime.now())

    def requires(self):
        # in this case just an ECS task is yielded but many more may be called
        # any input parameters should be defined on the class level like date
        yield DumbHelloWorld(date=self.date)

