"""
ecs.py

This is a modified version of the luigi.contrib.ecs module originally written by Jake Feala for Outlier Bio.
The original code is Copyright 2015 Outlier Bio, LLC
The original code is here: https://github.com/spotify/luigi/blob/master/luigi/contrib/ecs.py (apache 2 license)

Modifications here are Copyright 2017 Jarrett Egertson
"""

import time
import logging
import luigi

logger = logging.getLogger('luigi-interface')

try:
    import boto3
    client = boto3.client('ecs')
except ImportError:
    logger.warning('boto3 is not installed. ECSTasks require boto3')

POLL_TIME = 2


def _get_task_statuses(task_ids, cluster_name):
    """
    Retrieve task statuses from ECS API
    Returns list of {RUNNING|PENDING|STOPPED} for each id in task_ids
    """
    response = client.describe_tasks(cluster=cluster_name, tasks=task_ids)

    # Error checking
    if response['failures'] != []:
        raise Exception('There were some failures:\n{0}'.format(
            response['failures']))
    status_code = response['ResponseMetadata']['HTTPStatusCode']
    if status_code != 200:
        msg = 'Task status request received status code {0}:\n{1}'
        raise Exception(msg.format(status_code, response))

    return [t['lastStatus'] for t in response['tasks']]


def _track_tasks(task_ids, cluster_name):
    """Poll task status until STOPPED"""
    while True:
        statuses = _get_task_statuses(task_ids=task_ids, cluster_name=cluster_name)
        if all([status == 'STOPPED' for status in statuses]):
            logger.info('ECS tasks {0} STOPPED'.format(','.join(task_ids)))
            break
        time.sleep(POLL_TIME)
        logger.debug('ECS task status for tasks {0}: {1}'.format(
            ','.join(task_ids), status))


class ECSTask(luigi.Task):
    """
    Base class for an Amazon EC2 Container Service Task
    Amazon ECS requires you to register "tasks", which are JSON descriptions
    for how to issue the ``docker run`` command. This Luigi Task can either
    run a pre-registered ECS taskDefinition, OR register the task on the fly
    from a Python dict.
    :param task_def_arn: pre-registered task definition ARN (Amazon Resource
        Name), of the form::
            arn:aws:ecs:<region>:<user_id>:task-definition/<family>:<tag>
    :param task_def: dict describing task in taskDefinition JSON format, for
        example::
            task_def = {
                'family': 'hello-world',
                'volumes': [],
                'containerDefinitions': [
                    {
                        'memory': 1,
                        'essential': True,
                        'name': 'hello-world',
                        'image': 'ubuntu',
                        'command': ['/bin/echo', 'hello world']
                    }
                ]
            }
    """

    task_def_arn = luigi.Parameter(default=None)
    task_def = luigi.DictParameter(default=None)
    cluster_name = luigi.Parameter(default="default")

    @property
    def ecs_task_ids(self):
        """Expose the ECS task ID"""
        if hasattr(self, '_task_ids'):
            return self._task_ids

    @property
    def command(self):
        """
        Command passed to the containers
        Override to return list of dicts with keys 'name' and 'command',
        describing the container names and commands to pass to the container.
        Directly corresponds to the `overrides` parameter of runTask API. For
        example::
            [
                {
                    'name': 'myContainer',
                    'command': ['/bin/sleep', '60']
                }
            ]
        """
        pass

    def run(self):
        # override this if you want to add other stuff after run_ecs
        self.run_ecs()

    def run_ecs(self):
        if (not self.task_def and not self.task_def_arn) or \
           (self.task_def and self.task_def_arn):
            raise ValueError(('Either (but not both) a task_def (dict) or'
                              'task_def_arn (string) must be assigned'))
        if not self.task_def_arn:
            # Register the task and get assigned taskDefinition ID (arn)
            response = client.register_task_definition(**self.task_def)
            self.task_def_arn = response['taskDefinition']['taskDefinitionArn']

        # Submit the task to AWS ECS and get assigned task ID
        # (list containing 1 string)
        if self.command:
            overrides = {'containerOverrides': self.command}
        else:
            overrides = {}
        response = client.run_task(cluster=self.cluster_name,
                                   taskDefinition=self.task_def_arn,
                                   overrides=overrides)
        self._task_ids = [task['taskArn'] for task in response['tasks']]

        # Wait on task completion
        _track_tasks(task_ids=self._task_ids, cluster_name=self.cluster_name)
